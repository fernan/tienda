#!/usr/bin/env python3

'''
Maneja un diccionario con los artículos de una tienda y
sus precios, y permite luego comprar alguno de sus elementos,
y ver el precio total de la compra.
'''
import sys

articulos = {}


def anadir(articulo, precio):
    """Añade un artículo y un precio al diccionario artículos"""
    articulos[articulo] = precio


def mostrar():
    """Muestra en pantalla todos los artículos y sus precios"""
    print("Lista de artículos en la tienda:")
    for articulo, precio in articulos.items():
        print(f"{articulo}: {precio} euros")


def pedir_articulo() -> str:
    """Pide al usuario el artículo a comprar.

    Muestra un mensaje pidiendo el artículo a comprar.
    Cuando el usuario lo escribe, comprueba que es un artículo que
    está en el diccionario artículos, y termina devolviendo ese
    artículo como string. Si el artículo escrito por el usuario no
    está en artículos, pide otro hasta que escriba uno que sí está."""

    while True:
        articulo = input("Artículo: ")
        if articulo in articulos:
            return articulo
        else:
            print("El artículo no está en la lista, por favor, elige uno de la lista.")


def pedir_cantidad() -> float:
    """Pide al usuario la cantidad a comprar.

    Muestra un mensaje pidiendo la cantidad a comprar.
    Cuando el usuario lo escribe, comprueba que es un número real
    (float), y termina devolviendo esa cantidad. Si la cantidad
    escrita no es un float, pide otra hasta que escriba una correcta.
    """

    while True:
        cantidad = input("Cantidad: ")
        try:
            cantidad = float(cantidad)
            return cantidad
        except ValueError:
            print("La cantidad no es un número válido, por favor, introduce un número.")


def main():
    """Toma los artículos declarados como argumentos en la línea de comando,
    junto con sus precios, almacénalos en el diccionario artículos mediante
    llamadas a la función añadir. Luego muestra el listado de artículos
    y precios para que el usuario pueda elegir. Termina llamando a las
    funciones pedir_articulo y pedir cantidad, y mostrando en pantalla la
    cantidad comprada, de qué artículo, y cuánto es su precio.
    """

    args = sys.argv[1:]
    if len(args) % 2 != 0:
        print("Error en argumentos: Los argumentos deben estar en pares de artículo-precio.")
        sys.exit(1)

    for i in range(0, len(args), 2):
        articulo = args[i]
        precio = args[i + 1]
        if not precio.replace('.', '', 1).isdigit():
            print(f"Error en argumentos: {precio} no es un precio válido.")
            sys.exit(1)
        anadir(articulo, float(precio))

    if not articulos:
        print("Error en argumentos: No se han especificado artículos.")
        sys.exit(1)

    mostrar()
    articulo_comprar = pedir_articulo()
    cantidad_comprar = pedir_cantidad()
    precio_total = articulos[articulo_comprar] * cantidad_comprar

    print(f"Compra total: {cantidad_comprar} de {articulo_comprar}, a pagar {precio_total:.2f} euros")


if __name__ == '__main__':
    main()

